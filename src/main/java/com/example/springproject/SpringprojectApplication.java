package com.example.springproject;

import com.example.springproject.dao.CustomerDAO;
import com.example.springproject.model.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;
import java.util.List;

@SpringBootApplication
public class SpringprojectApplication {

    public static void main(String[] args) throws SQLException {
        SpringApplication.run(SpringprojectApplication.class, args);

        CustomerDAO customerDAO  = new CustomerDAO();
        List<Customer> customerList = customerDAO.getCustomers();
        customerList.forEach(System.out::println);
    }

}
