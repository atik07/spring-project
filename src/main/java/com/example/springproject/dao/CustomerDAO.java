package com.example.springproject.dao;

import com.example.springproject.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {



    public List<Customer> getCustomers() throws SQLException {

        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/trainee", "root", "root@786");
        Statement stmt = con.createStatement();
        ResultSet rs=stmt.executeQuery("select *from customer");

        List<Customer> customers = new ArrayList<>();

        while(rs.next()){

            int id = rs.getInt("id");
            String name = rs.getString("name");
            String address = rs.getString("address");;

            Customer customer = new Customer(id,name,address);
            customers.add(customer);
        }

    return customers;
    }

}